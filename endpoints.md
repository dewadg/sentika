## End-point

##### Registrasi Peserta
- End-point: `/api/registration`
- Method: POST
- Parameter:
    - email: wajib
    - password: wajib
    - password_conf: wajib, harus sama dengan password
    - full_name: wajib
    - phone: wajib
    - address: wajib
    - organization: opsional
    - organization_address: opsional
    - products: wajib, array, ID dari Product