<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Registrar;

class RegistrationSuccess extends Mailable
{
    use Queueable, SerializesModels;

    protected $registrar;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Registrar $registrar)
    {
        $this->registrar = $registrar;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $registrar = $this->registrar;
        $products = $this->registrar->products->map(function ($product) {
            return $product->name;
        })->toArray();

        return $this->view('mails.registration-success', compact('registrar', 'products'));
    }
}
