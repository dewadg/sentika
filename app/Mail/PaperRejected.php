<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Paper;

class PaperRejected extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Paper instance.
     *
     * @var Paper
     */
    protected $paper;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Paper $paper)
    {
        $this->paper = $paper;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $paper = $this->paper;
        $total = 0;
        $paper->registrar->products->each(function ($product) use (&$total) {
            $total += $product->price * $product->pivot->qty;
        });

        return $this->view('mails.paper-rejected', compact('paper', 'total'));
    }
}
