<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Paper;
use PDF;

class Loa extends Mailable
{
    use Queueable, SerializesModels;

    protected $paper;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Paper $paper)
    {
        $this->paper = $paper;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $paper = $this->paper;
        $pdf = PDF::loadView('pdf.loa', compact('paper'))->output();

        return $this->view('mails.loa', compact('paper'))
            ->attachData(
                $pdf,
                'loa.pdf',
                ['mime' => 'application/pdf',]
            );
    }
}
