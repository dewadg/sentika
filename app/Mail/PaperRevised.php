<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Paper;
use App\Product;
use App\Registrar;

class PaperRevised extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Paper instance
     *
     * @var Paper
     */
    protected $paper;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Paper $paper)
    {
        $this->paper = $paper;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $paper = $this->paper;
        $products = $paper->registrar->products;
        $total = 0;

        $sub_registrars = Registrar::whereNull('user_id')->whereNull('deleted_at')->where('registered_by', $paper->registrar->user_id)->get();
        
        $sub_registrars->map(function ($sub_registrar) use (&$products) {
            $sub_registrar->products->map(function ($product) use (&$products) {
                if ($product->id != 8)
                    $product->name = $product->name . " (Tambahan)";

                $toggle = true;
                $products->map(function ($p) use (&$product, &$toggle) {
                    if ($p->id == $product->id) {
                        $p->pivot->qty++;
                        $toggle = false;
                    }
                });           

                if ($toggle)
                    $products->push($product);
            });
        });
        
        $paper->registrar->products->each(function ($product) use (&$total) {
            $total += $product->price * $product->pivot->qty;
        });

        return $this->view('mails.paper-revised', compact('paper', 'total'));
    }
}
