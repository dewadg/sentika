<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Registrar extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'registered_by',
        'user_id',
        'full_name',
        'phone',
        'address',
        'organization',
        'organization_address'
    ];

    protected $dates = ['deleted_at'];

    protected $hidden = ['registered_by'/*, 'user_id'*/];

    public function registeredBy()
    {
        return $this->belongsTo(User::class, 'registered_by');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class)
            ->withPivot('qty', 'discount');
    }

    public function productsLogin()
    {
        return $this->belongsToMany(ProductLogin::class, 'product_registrar', 'registrar_id', 'product_id');
    }

    public function transaction()
    {
        return $this->hasOne(Transaction::class);
    }

    public function getSubRegistrars()
    {
        // if (is_null($this->registered_by)) {
        //     return $this->user->registrars->filter(function ($registrar) {
        //         return is_null($registrar->user_id);
        //     });
        // }
        
        // return [];

        //return $this->user->id;
        return Registrar::with([
                'products' => function ($query) {
                    $query->select(['id', 'name', 'price']);
                    $query->orderBy('id', 'DESC');
                },
            ])
                ->get()
                ->filter(function ($value) {
                    return is_null($value->user_id) && $value->registered_by == $this->user->id;
                })
                ->values();
    }

    public function parent()
    {
        return Registrar::get()
            ->filter(function ($value) {
                return !is_null($value->user_id) && $value->registered_by == $this->registered_by;
            })
            ->first();
    }

    public function papers() {
        return $this->hasMany(Paper::class);
    }
}
