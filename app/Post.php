<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'title',
        'slug',
        'content',
        'hidden'
    ];

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function sanitizeSlug($slug, $id = null)
    {
        $length = strlen($slug);
        $query = ! is_null($id) ? 'LEFT(slug, ' . $length . ') = "' . $slug . '" AND id != ' . $id : 'LEFT(slug, ' . $length . ') = "' . $slug . '"';
        $count = self::withTrashed()->whereRaw($query)->count();

        if ($count > 0) {
            return $slug . '-' . ($count + 1);
        }

        return $slug;
    }
}
