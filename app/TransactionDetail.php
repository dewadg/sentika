<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransactionDetail extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'transaction_id',
        'name',
        'qty',
        'price',
        'discount',
        'subtotal'
    ];

    protected $dates = ['deleted_at'];
}
