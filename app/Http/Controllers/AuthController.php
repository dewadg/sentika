<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $user_data = $request->only('email', 'password');
        $user_data['active'] = true;

        if (Auth::attempt($user_data)) {
            return redirect()
                ->route('panel');
        }

        return redirect()
            ->route('index.login')
            ->withErrors(collect(['Email/kata sandi tidak cocok']));
    }

    public function logout()
    {
        Auth::logout();

        return redirect()
            ->route('index.login');
    }
}
