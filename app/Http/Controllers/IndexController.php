<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
	public function index()
	{
		return view('home');
	}

    public function login()
    {
        return view('login');
    }

    public function panel()
    {
        $jwt = Auth::user()->generateJwt();

        return view('panel', compact('jwt'));
    }
}
