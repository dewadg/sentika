<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Image;
use Validator;
use DB;

class ImageController extends Controller
{
    public function get($id)
    {
        $image = Image::find($id);
        $image->url = $image->url();
        
        if (is_null($image)) {
            return response()
                ->json(['errors' => ['image_not_found']], 404);
        }

        return $image;
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'files' => 'required|file',
        ]);

        if ($validation->fails()) {
            return response()->json(['errors' => $validation->errors()], 422);
        }

        $files = $request->files->get('files');

        try {
            DB::transaction(function () use ($files, &$image) {
                $file_name = Image::sanitizeFileName(
                    $files->getClientOriginalName(),
                    $files->getClientOriginalExtension()
                );
                Storage::putFileAs('public/images', $files, $file_name);

                $image = Image::create([
                    'file_name' => $file_name,
                    'extension' => $files->getClientOriginalExtension(),
                    'size' => 1/*$files->getClientSize()*/,
                ]);
                $image->url = $image->url();
            });

            return $image;
        } catch (\Exception $e) {
            return response()->json(['errors' => [$e->getMessage()]], 500);
        }
    }
}
