<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use NumberToWords\NumberToWords;
use PDF;

class ReceiptController extends Controller
{
    public function seminar(Request $request)
    {
        $transformer = (new NumberToWords)->getNumberTransformer('id');
        $transactions = collect($this->dummyData())
            ->map(function ($transaction) use ($transformer) {
                $transaction['total_in_words'] = $transformer->toWords($transaction['total']) . ' Rupiah';

                return $transaction;
            });
        $description = 'Biaya Peserta SENTIKA 2018';

        $pdf = PDF::loadView('pdf.receipts', compact('transactions', 'description'));

        return $pdf->stream();
    }
}
