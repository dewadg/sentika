<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Mail\PaperRejected;
use App\Mail\PaperRevised;
use App\Mail\Loa;
use App\File;
use App\Paper;
use App\PaperUpload;
use App\PaperStatus;
use App\PaperLetter;
use Validator;
use DB;
use PDF;

class PaperController extends Controller
{
    public function index()
    {
        $paper = Paper::with([
            'registrar' => function($query) {
                $query->select(['id', 'user_id', 'full_name']);
            },
            'logs' => function ($query) {
                $query->select(['id', 'name']);
            },
        ])
            ->orderBy('created_at', 'DESC')
            ->get()
            ->filter(function ($paper) {
                return ! is_null($paper->registrar);
            })
            ->map(function ($value) {
                $value->uploads = PaperUpload::with(['files'])->where('paper_id', $value->id)->first();

                if (empty($value->logs[0])) {
                    $logs = [                    
                        'name' => '-',
                    ];
                    $value->logs[0] = $logs;
                }

                return $value;
            })
            ->filter(function ($value) {
                return !is_null($value->uploads);
            })
            ->values();

        return $paper;
    }

    public function get($id)
    {
        $paper = Paper::with([
            'registrar' => function($query) {
                $query->select(['id', 'user_id', 'full_name']);
            },
            'logs' => function ($query) {
                $query->select(['id', 'name']);
            },
            'additionalAuthors' => function ($query) {
                $query->select(['id', 'full_name']);
            },
            'review' => function ($query) {
                $query->select(['id', 'paper_id']);
            },
            'letter' => function ($query) {
                $query->select(['id', 'paper_id', 'file_name', 'created_at']);
            },
        ])->find($id);

        $paper->uploads = PaperUpload::with([
            'files' => function ($query) {
                $query->orderBy('created_at', 'DESC');
            },
        ])->where('paper_id', $paper->id)->first();

        $paper->uploads->files->map(function ($value) {
            $value->url = $value->url();
        });

        if ($paper->letter) {
            $paper->letter->url = $paper->letter->url();
        }

        if ($paper->uploads != null) {
            return $paper;
        }else{
            return response()->json([]);
        }
    }

    public function getByRegistrar($id)
    {
        $paper = Paper::with([
            'registrar' => function($query) {
                $query->select(['id', 'user_id', 'full_name']);
            },
            'logs' => function ($query) {
                $query->select(['id', 'name']);
            },
        ])
            ->where('registrar_id', $id)
            ->orderBy('created_at', 'DESC')
            ->get();

        $paper->map(function ($e) {
            $e->uploads = PaperUpload::with([
                'files' => function ($query) {
                    $query->orderBy('created_at', 'DESC');
                },
            ])->where('paper_id', $e->id)->first();

            if ($e->uploads) {
                $e->uploads->files->map(function ($value) {
                    $value->url = $value->url();
                });
            }

            if (empty($e->logs[0])) {
                $logs = [                    
                    'name' => '-',
                ];
                $e->logs[0] = $logs;
            }
        });

        $paper = $paper->filter(function ($value) {
            return !is_null($value->uploads);
        })
            ->values();

        return $paper;
    }

    public function getProceeding()
    {
        $paper = Paper::with([
            'registrar' => function($query) {
                $query->select(['id', 'user_id', 'full_name']);
            },
            'logs' => function ($query) {
                $query->select(['id', 'name']);
            },
        ])
            ->orderBy('created_at', 'DESC')
            ->get()
            ->map(function ($value) {
                $value->uploads = PaperUpload::select(['id', 'paper_id'])->where('paper_id', $value->id)->first();

                return $value;
            })
            ->filter(function ($value) {
                if (!empty($value->logs[0])) {
                    return $value->logs[0]->id == 7 && !is_null($value->uploads);
                }
            })
            ->values();

        return $paper;
    }

    public function getStatuses()
    {
        $paper_status = PaperStatus::all();

        return $paper_status;
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'registrar_id' => 'required',
            'user_id' => 'required',
            'title' => 'required',
        ]);

        if ($validation->fails()) {
            return response()->json(['errors' => $validation->errors()], 422);
        }

        try {
            $paper_data = [
                'id' => Paper::createId(),
                'registrar_id' => $request->get('registrar_id'),
                'title' => $request->get('title'),
            ];

            $log_data = [[
                'paper_status_id' => 1, 
                'related_user_id' => $request->get('user_id')
            ],];

            DB::transaction(function () use ($paper_data, $log_data, &$paper, &$upload, &$file) {
                $paper = Paper::create($paper_data);
                $paper->logs()->attach($log_data);
            });

            if ($this->upload($request, $paper_data['id']))
                return $this->get($paper_data['id']);
            else {
                $paper->delete();
                $paper->logs()->detach();
                return response()->json([]);
            }

        } catch (\Exception $e) {
            return response()->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function storeLog(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'paper_id' => 'required',
            'paper_status_id' => 'required',
            'user_id' => 'required',
        ]);

        if ($validation->fails()) {
            return response()->json(['errors' => $validation->errors()], 422);
        }

        try {
            $paper_id = $request->get('paper_id');

            $log_data = [[
                'paper_status_id' => $request->get('paper_status_id'), 
                'related_user_id' => $request->get('user_id'),
            ],];

            DB::transaction(function () use ($paper_id, $log_data, &$paper) {
                $paper = Paper::find($paper_id);
                $paper->logs()->attach($log_data);
            });

            $return_values = $this->get($paper_id);

            return $return_values;
        } catch (\Exception $e) {
            return response()->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function storeAuthor(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'paper_id' => 'required',
            'registrar_id' => 'required',
        ]);

        if ($validation->fails()) {
            return response()->json(['errors' => $validation->errors()], 422);
        }

        try {
            $author_data = [[
                'paper_id' => $request->get('paper_id'), 
                'registrar_id' => $request->get('registrar_id'),
            ],];

            DB::transaction(function () use ($author_data, &$paper) {
                $paper = Paper::find($author_data[0]['paper_id']);
                $paper->additionalAuthors()->attach($author_data);
            });

            $return_values = $this->get($author_data[0]['paper_id']);

            return $return_values;
        } catch (\Exception $e) {
            return response()->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function upload(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'file' => 'required|file',
        ]);

        if ($validation->fails()) {
            return response()->json(['errors' => $validation->errors()], 422);
        }

        try {
            $file_req = $request->files->get('file');
            $file_name = File::sanitizeFileName(
                $file_req->getClientOriginalName(),
                $file_req->getClientOriginalExtension()
            );
            Storage::putFileAs('public/files', $file_req, $file_name);

            $upload_data = [
                'paper_id' => $id,
                'file_name' => $file_name,
                'size' => 1/*$file_req->getClientSize()*/,
            ];

            $file_data = [
                'file_name' => $file_name,
            ];

            $upload = PaperUpload::where('paper_id', $id)->first();

            $isCamReady = 0;
            if ($request->get('cam_ready'))
                $isCamReady = $request->get('cam_ready');


            DB::transaction(function () use ($upload_data, $upload, $file_data, $isCamReady, &$file) {
                if (!$upload) {
                    $upload = PaperUpload::create($upload_data);
                }else{
                    $upload->update($upload_data);
                }

                if ($isCamReady == 1)
                    $file_data['rev'] = File::getRevNumber($upload->id) + 100;
                else
                    $file_data['rev'] = File::getRevNumber($upload->id);

                $file = [];
                array_push($file, new File($file_data));

                $upload->files()->saveMany($file);
            });

            $return_values = $this->get($id);

            return $return_values;
        } catch (\Exception $e) {
            return response()->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function uploadLetter(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'file' => 'required|file',
        ]);

        if ($validation->fails()) {
            return response()->json(['errors' => $validation->errors()], 422);
        }

        try {
            $file_req = $request->files->get('file');
            $file_name = PaperLetter::sanitizeFileName(
                $file_req->getClientOriginalName(),
                $file_req->getClientOriginalExtension()
            );
            Storage::putFileAs('public/files', $file_req, $file_name);

            $letter_data = new PaperLetter([
                'paper_id' => $id,
                'file_name' => $file_name,
            ]);

            $paper = Paper::find($id);

            DB::transaction(function () use ($letter_data, $paper) {
                $paper->letter()->save($letter_data);
            });

            $return_values = $this->get($id);

            return $return_values;
        } catch (\Exception $e) {
            return response()->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function update(Request $request, $id)
    {
        $paper = Paper::find($id);

        if (is_null($paper)) {
            return response()
                ->json(['errors' => ['paper_not_found']], 404);
        }

        try {
            $user_data['paper'] = $request->all();

            DB::transaction(function () use ($paper, $user_data) {
                $paper->update($user_data['paper']);
            });

            $return_values = $this->get($id);

            return $return_values;
        } catch (\Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function updateAuthor(Request $request, $id)
    {
        $paper = Paper::find($id);

        if (is_null($paper)) {
            return response()
                ->json(['errors' => ['paper_not_found']], 404);
        }

        try {
            $user_data['authors'] = $request->get('authors');

            DB::transaction(function () use ($paper, $user_data) {
                if(!is_null($user_data['authors']))
                    $paper->additionalAuthors()->sync($user_data['authors']);
                else
                    $this->destroyAuthor($id);
            });

            $return_values = $paper->additionalAuthors;

            return $return_values;
        } catch (\Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function updateLog(Request $request, $id)
    {
        $paper = Paper::find($id);

        if (is_null($paper)) {
            return response()
                ->json(['errors' => ['paper_not_found']], 404);
        }

        try {
            $logs = $request->get('logs');
            $i = 0;
            foreach ($logs as $log){ 
                $log_data[$i]['paper_status_id'] = $log['paper_status_id'];
                $log_data[$i]['related_user_id'] = $log['related_user_id'];
                $i++;
            }

            DB::transaction(function () use ($paper, $log_data) {
                $this->destroyLog($paper->id);
                $paper->logs()->attach($log_data);
            });

            $return_values = $paper->logs;

            return $return_values;
        } catch (\Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function destroy($id)
    {
        $paper = Paper::find($id);
        $upload = PaperUpload::where('paper_id', $id)->first();
        
        if (is_null($paper)) {
            return response()
                ->json(['errors' => ['paper_not_found']], 404);
        }

        try {
            DB::transaction(function () use ($paper, $upload, $id) {
                DB::statement('SET FOREIGN_KEY_CHECKS = 0');
                $paper->logs()
                    ->detach();
                if ($paper->review) {
                    $paper->review
                        ->delete();
                }                
                $paper->delete();
                $upload->files
                    ->each->delete();
                $upload->delete();
                DB::statement('SET FOREIGN_KEY_CHECKS = 1');
            });
            
            return response()->json([]);
        } catch (\Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function destroyAuthor($id)
    {
        $paper = Paper::find($id);
        
        if (is_null($paper)) {
            return response()
                ->json(['errors' => ['paper_not_found']], 404);
        }

        try {
            DB::transaction(function () use ($paper) {
                DB::statement('SET FOREIGN_KEY_CHECKS = 0');
                $paper->additionalAuthors()
                    ->detach();
                DB::statement('SET FOREIGN_KEY_CHECKS = 1');
            });
            
            return response()->json([]);
        } catch (\Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function destroyLog($id)
    {
        $paper = Paper::find($id);
        
        if (is_null($paper)) {
            return response()
                ->json(['errors' => ['paper_not_found']], 404);
        }

        try {
            DB::transaction(function () use ($paper) {
                DB::statement('SET FOREIGN_KEY_CHECKS = 0');
                $paper->logs()
                    ->detach();
                DB::statement('SET FOREIGN_KEY_CHECKS = 1');
            });
            
            return response()->json([]);
        } catch (\Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function destroyLetter($id)
    {
        $paper = Paper::find($id);
        
        if (is_null($paper)) {
            return response()
                ->json(['errors' => ['paper_not_found']], 404);
        }

        try {
            DB::transaction(function () use ($paper) {
                DB::statement('SET FOREIGN_KEY_CHECKS = 0');
                if ($paper->letter) {
                    $paper->letter
                        ->delete();
                } 
                DB::statement('SET FOREIGN_KEY_CHECKS = 1');
            });
            
            return response()->json([]);
        } catch (\Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function countByRegistrar($id)
    {
        $paper = $this->getByRegistrar($id);

        return $paper->count();
    }

    public function sendRejectionEmail($id)
    {
        $paper = Paper::with([
            'registrar',
            'registrar.products',
            'review',
        ])
            ->find($id);

        if (is_null($paper)) {
            return response()->json(['errors' => ['paper_not_found']], 404);
        }

        try {
            Mail::to($paper->registrar->user->email)->bcc('sentika@uajy.ac.id')->send(new PaperRejected($paper));

            return response()->json();
        } catch (\Exception $e) {
            return response()->json([$e->getMessage()], 500);
        }
    }

    public function sendRevisionEmail($id)
    {
        $paper = Paper::with([
            'registrar',
            'registrar.products',
            'review',
        ])
            ->find($id);

        if (is_null($paper)) {
            return response()->json(['errors' => ['paper_not_found']], 404);
        }

        try {
            Mail::to($paper->registrar->user->email)->bcc('sentika@uajy.ac.id')->send(new PaperRevised($paper));

            return response()->json();
        } catch (\Exception $e) {
            return response()->json([$e->getMessage()], 500);
        }
    }

    public function sendLoaEmail($id)
    {
        $paper = Paper::with(['registrar'])->find($id);

        if (is_null($paper)) {
            return response()->json(['errors' => ['paper_not_found']], 404);
        }

        try {
            Mail::to($paper->registrar->user->email)->bcc('sentika@uajy.ac.id')->send(new Loa($paper));

            return response()->json();
        } catch (\Exception $e) {
            return response()->json([$e->getMessage()], 500);
        }
    }

    public function testEmailView($id)
    {
        $paper = Paper::with([
            'registrar',
            'registrar.products',
            'review',
        ])
            ->find($id);
        $total = 0;
        $paper->registrar->products->each(function ($product) use (&$total) {
            $total += $product->price * $product->pivot->qty;
        });

        return view('mails.paper-revised', compact('paper', 'total'));
    }

    public function testLoaView($id)
    {
        $paper = Paper::with(['registrar'])->find($id);

        return PDF::loadView('pdf.loa', compact('paper'))->stream();
    }
}
