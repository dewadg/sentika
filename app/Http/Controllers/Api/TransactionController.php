<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Transaction;
use App\TransactionDetail;
use App\Registrar;
use Validator;
use NumberToWords\NumberToWords;
use PDF;
use DB;

class TransactionController extends Controller
{
    public function index()
    {
        return Transaction::with([
            'registrar' => function ($query) {
                $query->select(['id', 'full_name']);
            },
            'verifiedBy' => function ($query) {
                $query->select(['id', 'email']);
            },
            'details' => function ($query) {
                $query->select(['id', 'transaction_id', 'name', 'qty', 'price', 'discount', 'subtotal']);
            },
            'paymentProof' => function ($query) {
                $query->select(['id', 'file_name']);
            },
        ])
            ->get()
            ->map(function ($value) {
                $value->paymentProof->url = $value->paymentProof->url();

                return $value;
            })
            ->filter(function ($value) {
                return !is_null($value->registrar) && !is_null($value->details);
            })
            ->values();
    }

	public function get($id)
    {
        $transaction = Transaction::with([
            'registrar' => function ($query) {
                $query->select(['id', 'full_name']);
            },
            'verifiedBy' => function ($query) {
                $query->select(['id', 'email']);
            },
            'details' => function ($query) {
                $query->select(['id', 'transaction_id', 'name', 'qty', 'price', 'discount', 'subtotal']);
            },
            'paymentProof' => function ($query) {
                $query->select(['id', 'file_name']);
            },
        ])->find($id);

        $transaction->paymentProof->url = $transaction->paymentProof->url();
        
        if (is_null($transaction) && is_null($transaction->details)) {
            return response()
                ->json(['errors' => ['transaction_not_found']], 404);
        }

        return $transaction;
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
        	// Validation for Transaction
            'registrar_id' => 'required',
            'total' => 'required|integer',
            'payment_proof_id' => 'required',
            // Validation for TransactionDetail
            'details.*.name' => 'required',
            'details.*.qty' => 'required|integer',
            'details.*.price' => 'required|integer',
            'details.*.discount' => 'required|integer',
            'details.*.subtotal' => 'required|integer',
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }

        $user_data = [
            'user_id' => $request->get('user_id'),
            'registrar_id' => $request->get('registrar_id'),
            'total' => $request->get('total'),
            'payment_proof_id' => $request->get('payment_proof_id'),
        ];

        $details = [];
        foreach ($request->get('details') as $detail) {
            array_push($details, new TransactionDetail($detail));
        }

        try {
            DB::transaction(function () use ($user_data, $details, &$transaction) {
                $transaction = Transaction::create($user_data);
                $transaction->details()
                    ->saveMany($details);
            });

            return Transaction::with(['details'])->find($transaction->id);
        } catch (\Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function update(Request $request, $id)
    {
        $transaction = Transaction::find($id);
        
        if (is_null($transaction)) {
            return response()
                ->json(['errors' => ['transaction_not_found']], 404);
        }

        $validation = Validator::make($request->all(), [
            'verified_by' => 'required',
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }

        $user_data = $request->all();

        try {
            DB::transaction(function () use ($user_data, $transaction) {
                $transaction->update($user_data);
            });

            return $this->get($id);
        } catch (\Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function destroy($id)
    {
        $transaction = Transaction::find($id);
        
        if (is_null($transaction)) {
            return response()
                ->json(['errors' => ['transaction_not_found']], 404);
        }

        $transaction->details
            ->each->delete();
        $transaction->delete();

        return response()->json();
    }

    protected function receipt(Transaction $transaction)
    {
        $transformer = (new NumberToWords)->getNumberTransformer('id');
        $transaction->total_in_words = $transformer->toWords((int) $transaction['total']) . ' Rupiah';
        $description = 'Biaya Peserta SENTIKA 2018';

        $transactions = [$transaction];

        return PDF::loadView('pdf.receipts', compact('transactions', 'description'));
    }

    protected function loa(Registrar $registrar)
    {
        return PDF::loadView('pdf.loa', compact('registrar'));
    }

    public function generateReceipt($id)
    {
        $transaction = Transaction::with([
            'registrar',
            'registrar.papers',
        ])->find($id);

        return $this->loa($transaction->registrar)->stream();
    }
}
