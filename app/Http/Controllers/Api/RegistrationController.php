<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Mail\RegistrationSuccess;
use App\Jobs\SendRegistrationSuccessEmail;
use App\User;
use App\Registrar;
use Validator;
use DB;

class RegistrationController extends Controller
{
    public function register(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'password_conf' => 'required|same:password',
            'full_name' => 'required',
            'phone' => 'required|max:12',
            'address' => 'required',
            'products' => 'required|array'
        ]);

        if ($validation->fails()) {
            return response()
                ->json(['errors' => $validation->errors()], 422);
        }

        $user_data['user'] = [
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'role_id' => 4,
            'active' => true,
        ];
        $user_data['registrar'] = [
            'full_name' => $request->get('full_name'),
            'phone' => $request->get('phone'),
            'address' => $request->get('address'),
            'organization' => empty($request->get('organization')) ? null : $request->get('organization'),
            'organization_address' => empty($request->get('organization_address')) ? null : $request->get('organization_address'),
            //'products' => $request->get('products')
        ];

        $extra_data = array_map(function($e)  {
            return [
                'qty' => 1,
                'discount' => 0,
            ];
        }, $request->get('products'));

        $user_data['registrar']['products'] = array_combine($request->get('products'), $extra_data);

        try {
            DB::transaction(function () use ($user_data) {
                $user = User::create($user_data['user']);
                $user_data['registrar']['registered_by'] = $user->id;
                $user_data['registrar']['user_id'] = $user->id;
                $registrar = Registrar::create($user_data['registrar']);
                $registrar->products()->attach($user_data['registrar']['products'], ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);

                $registrar = Registrar::with(['user', 'products'])->find($registrar->id);

                if (env('APP_ENV') != 'local') {
                    if (! env('MAIL_JOB')) {
                        Mail::to($registrar->user->email)->send(new RegistrationSuccess($registrar));                    
                    } else {
                        SendRegistrationSuccessEmail::dispatch($registrar);
                    }
                }
            });

            return response()->json();
        } catch (\Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }
}
