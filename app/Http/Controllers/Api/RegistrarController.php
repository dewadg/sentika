<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Registrar;
use Validator;
use DB;

class RegistrarController extends Controller
{
    public function index()
    {
        return Registrar::with([
            'user' => function ($query) {
                $query->select(['id', 'email']);
            },
            'registeredBy' => function ($query) {
                $query->select(['id', 'email']);
            },
            'products' => function ($query) {
                $query->select(['id', 'name']);
            },
            'transaction' => function ($query) {
                $query->select(['id','registrar_id','payment_proof_id']);
            },
        ])
            ->get()
            ->filter(function ($value) {
                return !is_null($value->user_id);
            })
            ->values();
    }

    public function getAllSubRegistrars()
    {
        $registrars = Registrar::with([
            'registeredBy' => function ($query) {
                $query->select(['id', 'email']);
            },
        ])
            ->get()
            ->filter(function ($value) {
                return is_null($value->user_id);
            })
            ->values();

        foreach ($registrars as $registrar) {
            $registrar->parent = $registrar->parent();
        }

        return $registrars;
    }

    public function get($id)
    {
        $registrar = Registrar::with([
            'registeredBy' => function ($query) {
                $query->select(['id', 'email', 'active']);
            }, 
            'user' => function ($query) {
                $query->select(['id', 'email', 'active']);
            }, 
            'products' => function ($query) {
                $query->select(['id', 'name', 'price']);
            },
            'transaction' => function ($query) {
                $query->select(['id','registrar_id','payment_proof_id']);
            },
        ])->find($id);

        if (is_null($registrar)) {
            return response()
                ->json(['errors' => ['registrar_not_found']], 404);
        }

        return $registrar;        
    }

    public function getPublicRegistrars()
    {
        $registrars = Registrar::with([
            'products' => function ($query) {
                $query->select(['id','name']);
                $query->orderBy('id', 'ASC');
            },
            'transaction' => function ($query) {
                $query->select(['id','registrar_id','verified_by']);
            },
        ])
            ->get()
            ->filter(function ($value) {
                foreach ($value->products as $product) {
                    return $product->id == 3 || $product->id == 4 || $product->id == 5;
                }
            })
            ->filter(function ($value) {
                if (!empty($value->transaction))
                    return !is_null($value->transaction->verified_by);
            })
            ->values();

        return $registrars;
    }

    public function getPublicAuthors()
    {
        $registrars = Registrar::with([
            'products' => function ($query) {
                $query->select(['id','name']);
                $query->orderBy('id', 'ASC');
            },
            'transaction' => function ($query) {
                $query->select(['id','registrar_id','verified_by']);
            },
        ])
            ->get()
            ->filter(function ($value) {
                foreach ($value->products as $product) {
                    return $product->id == 1 || $product->id == 2;
                }
            })
            ->filter(function ($value) {
                if (!empty($value->transaction))
                    return !is_null($value->transaction->verified_by);
            })
            ->values();

        $temp = [];
        foreach ($registrars as $registrar) {
            $subRegistrars = $registrar->getSubRegistrars();
            array_push($temp, $registrar);
            if (sizeof($subRegistrars) != 0) {
                foreach ($subRegistrars as $subRegistrar) {
                    array_push($temp, $subRegistrar);
                }
            }            
        }

        return $temp;
    }

    public function getSubRegistrarsByRegistrar($id)
    {
        $registrar = Registrar::find($id);

        if (is_null($registrar)) {
            return response()
                ->json(['errors' => ['registrar_not_found']], 404);
        }

        $subRegistrars = $registrar->getSubRegistrars();

        foreach ($subRegistrars as $subRegistrar) {
            $subRegistrar->parent = $subRegistrar->parent();
        }

        return $subRegistrars;
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'registered_by' => 'required',
            'full_name' => 'required',        
            'phone' => 'required|max:12',
            'address' => 'required',
            'organization' => 'required',
            'organization_address' => 'required',
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }        

        try {

            $user_data = [
                'registered_by' => $request->get('registered_by'),
                'full_name' => $request->get('full_name'),
                'phone' => $request->get('phone'),
                'address' => $request->get('address'),
                'organization' => $request->get('organization'),
                'organization_address' => $request->get('organization_address'),
            ]; 

            $user_data['products'] = null;
            if ($request->get('product_id')) {
                $products_ids = $request->get('product_id');
                $extra_data = array_map(function($e)  {
                    return [
                        'qty' => 1,
                        'discount' => 0,
                    ];
                }, $products_ids);

                $user_data['products'] = array_combine($products_ids, $extra_data);
            }

            DB::transaction(function () use ($user_data, &$registrar) {
                $registrar = Registrar::create($user_data);
                if ($user_data['products'])
                    $registrar->products()->sync($user_data['products']);
            });

            $return_values = $this->get($registrar->id);

            return $return_values;
        } catch (\Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function update(Request $request, $id)
    {
        $registrar = Registrar::find($id);

        if (is_null($registrar)) {
            return response()
                ->json(['errors' => ['registrar_not_found']], 404);
        }

        $validation = Validator::make($request->all(), [
            'full_name' => 'required',
            'phone' => 'required|max:12',
            'address' => 'required',
            //'product_id' => 'required|array',
        ]);

        if ($validation->fails()) {
            return response()
                ->json(['errors' => $validation->errors()], 422);
        }

        try {
            $user_data['registrar'] = $request->except('id', 'product_id', 'proceeding_qty', 'product_discount');
            $proceeding_qty = $request->get('proceeding_qty');
            $product_discount = $request->get('product_discount');

            $user_data['products'] = null;
            if ($request->get('product_id')) {
                $products_ids = $request->get('product_id');
                $extra_data = array_map(function($e) use ($proceeding_qty, $product_discount) {
                    if ($e == 6)
                        return [
                            'qty' => $proceeding_qty,
                            'discount' => 0,
                        ];
                    else
                        if ($e == 1)
                            return [
                                'qty' => 1,
                                'discount' => $product_discount,
                            ];
                        else
                            return [
                                'qty' => 1,
                                'discount' => 0,
                            ];
                }, $products_ids);

                $user_data['products'] = array_combine($products_ids, $extra_data);
            }

            DB::transaction(function () use (&$registrar, $user_data) {
                $registrar->update($user_data['registrar']);
                if ($user_data['products'])
                    $registrar->products()->sync($user_data['products']);
            });

            $return_values = $this->get($registrar->id);

            return $return_values;
        } catch (\Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function destroy($id)
    {
        $registrar = Registrar::with(['user', 'products'])->find($id);
        
        if (is_null($registrar)) {
            return response()
                ->json(['errors' => ['registrar_not_found']], 404);
        }

        try {
            DB::transaction(function () use ($registrar, $id) {
                DB::statement('SET FOREIGN_KEY_CHECKS = 0');
                $registrar->products()
                    ->detach();
                if ($registrar->user)
                    $registrar->user->delete();
                $registrar->delete();
                DB::statement('SET FOREIGN_KEY_CHECKS = 1');
            });
            
            return response()->json([]);
        } catch (\Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function countSubRegistrarsByRegistrar($id)
    {
        $registrar = Registrar::find($id);

        if (is_null($registrar)) {
            return response()
                ->json(['errors' => ['registrar_not_found']], 404);
        }

        return $registrar->getSubRegistrars()->count();
    }
}
