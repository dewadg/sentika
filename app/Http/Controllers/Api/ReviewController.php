<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Review;
use App\Registrar;
use Validator;
use DB;

class ReviewController extends Controller
{    
    public function index()
    {
        $review = Review::with([
            'paper' => function($query) {
                $query->select(['id', 'registrar_id', 'title']);
                $query->with(['logs']);
            },
            'reviewed_by' => function ($query) {
                $query->select(['id', 'email']);
            },
        ])
            ->get()
            ->map(function ($value) {
                $value->registrar = Registrar::select(['id', 'full_name'])->find($value->paper->registrar_id);

                if (empty($value->paper->logs[0])) {
                    $logs = [                    
                        'name' => '-',
                    ];
                    $value->paper->logs[0] = $logs;
                }

                return $value;
            });

        return $review;
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'paper_id' => 'required',
            'comment' => 'required'
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }

        $user_data = $request->all();

        try {
            DB::transaction(function () use ($user_data, &$review) {
                $review = Review::create($user_data);
            });

            return Review::find($review->id);
        } catch (\Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function get($id)
    {
        $review = Review::with([
            'paper' => function($query) {
                $query->select(['id', 'registrar_id', 'title']);
            },
            'reviewed_by' => function ($query) {
                $query->select(['id', 'email']);
            },
        ])->find($id);
        
        if (is_null($review)) {
            return response()
                ->json(['errors' => ['review_not_found']], 404);
        }

        return $review;
    }

    public function getByRegistrar($id)
    {
        $review = Review::with([
            'paper' => function($query) {
                $query->select(['id', 'registrar_id', 'title']);
            },
            'reviewed_by' => function ($query) {
                $query->select(['id', 'email']);
            },
        ])
            ->get()
            ->map(function ($value) {
                $value->registrar = Registrar::select(['id', 'full_name'])->find($value->paper->registrar_id);

                return $value;
            })
            ->filter(function ($value) use ($id) {
                return $value->paper->registrar_id == $id && $value->verified_by != null;
            })
            ->values();

        return $review;
    }

    public function update(Request $request, $id)
    {
        $review = Review::find($id);
        
        if (is_null($review)) {
            return response()
                ->json(['errors' => ['review_not_found']], 404);
        }

        $validation = Validator::make($request->all(), [
            'paper_id' => 'required',
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }

        $user_data = $request->all();

        try {
            DB::transaction(function () use ($user_data, $review) {
                $review->update($user_data);
            });

            return Review::find($review->id);
        } catch (\Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }
}
