<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use DB;

class ProfileController extends Controller
{
    public function changePassword(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'password' => 'required',
            'new_password' => 'required',
            'new_password_conf' => 'required|same:new_password'
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }

        $user = User::find($request->user->id);

        if (is_null($user)) {
            return response()
                ->json(['errors' => ['user_not_found']], 404);
        }

        if (! password_verify($request->get('password'), $user->password)) {
            return response()
                ->json(['errors' => ['password_mismatch']], 422);
        }

        try {
            DB::transaction(function () use ($user, $request) {
                $user->update([
                    'password' => bcrypt($request->get('new_password'))
                ]);
            });

            return response()
                ->json();
        } catch (\Exception $e) {
            return response()
                ->json(['errors' => ['internal_server_error']], 500);
        }
    }
}
