<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Download;
use Validator;
use DB;

class DownloadController extends Controller
{
    public function index()
    {
        return Download::get()->map(function ($download) {
            $download->url = $download->url();

            return $download;
        });
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'display_name' => 'required',
            'file' => 'required|file',
        ]);

        if ($validation->fails()) {
            return response()->json(['errors' => $validation->errors()], 422);
        }

        try {
            $user_data = [
                'display_name' => $request->get('display_name'),
                'file_name' => Download::sanitizeFileName(
                    $request->file->getClientOriginalName(),
                    $request->file->getClientOriginalExtension()
                ),
                'extension' => $request->file->getClientOriginalExtension(),
                'size' => $request->file->getClientSize(),
            ];

           $request->file->storeAs('public/downloads', $user_data['file_name']);

           DB::transaction(function () use ($user_data) {
               Download::create($user_data);
           });

           return response()->json([], 201);
        } catch (\Exception $e) {
            return response()->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function destroy($id)
    {
        $download = Download::find($id);

        if (is_null($download)) {
            return response()->json(['errors' => ['download_not_found']], 404);
        }

        Storage::delete('public/downloads/' . $download->file_name);
        $download->delete();
        
        return response()->json();
    }
}
