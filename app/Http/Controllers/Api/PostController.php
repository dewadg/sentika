<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use Validator;
use DB;

class PostController extends Controller
{
    public function index()
    {
        return Post::with('user')->get();
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'title' => 'required',
            'slug' => 'required',
            'content' => 'required',
            'hidden' => 'required'
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }

        $user_data = $request->all();
        $user_data['slug'] = Post::sanitizeSlug($user_data['slug']);
        $user_data['user_id'] = $request->user->id;

        try {
            DB::transaction(function () use ($user_data, &$post) {
                $post = Post::create($user_data);
            });

            return Post::with('user')->find($post->id);
        } catch (\Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function get($id)
    {
        $post = Post::find($id);
        
        if (is_null($post)) {
            return response()
                ->json(['errors' => ['post_not_found']], 404);
        }

        return $post;
    }

    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        
        if (is_null($post)) {
            return response()
                ->json(['errors' => ['post_not_found']], 404);
        }

        $validation = Validator::make($request->all(), [
            'title' => 'required',
            'slug' => 'required',
            'content' => 'required',
            'hidden' => 'required'
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }

        $user_data = $request->all();
        $user_data['slug'] = Post::sanitizeSlug($user_data['slug'], $id);

        try {
            DB::transaction(function () use ($user_data, $post) {
                $post->update($user_data);
            });

            return Post::find($post->id);
        } catch (\Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function destroy($id)
    {
        $post = Post::find($id);
        
        if (is_null($post)) {
            return response()
                ->json(['errors' => ['post_not_found']], 404);
        }

        $post->delete();

        return response()->json();
    }

    public function bySlug($slug)
    {
        $post = Post::where('slug', $slug)->first();

        if (is_null($post)) {
            return response()->json(['errors' => ['post_not_found']], 404);
        }

        return $post;
    }
}
