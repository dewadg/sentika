<?php

namespace App\Http\Middleware;

use Closure;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class JwtMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (empty($request->header('Authorization'))) {
            return response()
                ->json([
                    'errors' => ['token_required']
                ], 400);
        }

        $token = $this->validateToken(explode(' ', $request->header('Authorization'))[1]);

        if (! is_null($token)) {
            $request->user = $token->getClaim('user');

            return $next($request);
        }

        return response()
            ->json([
                'errors' => ['invalid_token']
            ], 400);
    }

    public function validateToken($token)
    {
        $token = (new Parser)->parse($token);
        $signer = new Sha256;

        if ($token->verify($signer, env('JWT_SIGN_KEY'))) {
            return $token;
        }

        return null;
    }
}
