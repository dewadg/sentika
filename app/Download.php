<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Download extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'file_name',
        'display_name',
        'extension',
        'size',
    ];

    public static function sanitizeFileName($file_name, $extension)
    {
        $name = basename($file_name, '.' . $extension);
        $length = strlen($name);
        $duplicate_files = self::withTrashed()->whereRaw('LEFT(file_name, ' . $length . ') = "' . $name . '"')->count();

        if ($duplicate_files > 0) {
            return $name . '_' . ($duplicate_files + 1) . '.' . $extension;
        }

        return $file_name;
    }

    public function url()
    {
        return url('/storage/downloads/' . $this->file_name);
    }
}
