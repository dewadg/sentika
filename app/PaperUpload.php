<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaperUpload extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'paper_id',
        'file_name',
        'size'
    ];

    protected $dates = ['deleted_at'];    

    public function files()
    {
        return $this->hasMany(File::class, 'paper_upload_id');
    }

    public function latestFile()
    {
        return $this->files->sortByDesc('created_at')->first();
    }
}
