<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaperLetter extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'paper_id',
        'file_name',
    ];

    protected $dates = ['deleted_at'];

    public function paper()
    {
        return $this->belongsTo(Paper::class);
    }

    public function url()
    {
        return url('/storage/files/' . $this->file_name);
    }

    public static function sanitizeFileName($file_name, $extension)
    {
        $name = basename($file_name, '.' . $extension);
        $length = strlen($name);
        $duplicate_files = self::withTrashed()->whereRaw('LEFT(file_name, ' . $length . ') = "' . $name . '"')->count();

        if ($duplicate_files > 0) {
            return $name . '_' . ($duplicate_files + 1) . '.' . $extension;
        }

        return $file_name;
    }
}
