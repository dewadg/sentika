<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id', 'email', 'password', 'active'
    ];

    protected $casts = [
        'active' => 'boolean'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'role_id'
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function generateJwt()
    {
        $user = User::find($this->id)->registrars()->with([
            'productsLogin' => function ($query) { 
                $query->select(['id']);
            },
        ])->first();
        
        $signer = new Sha256;
        $token = (new Builder)
            ->setIssuer(env('APP_URL'))
            ->setAudience(env('APP_URL'))
            ->setIssuedAt(time())
            ->set('user', User::with('role')->find($this->id))
            ->set('registrar', $user)
            ->sign($signer, env('JWT_SIGN_KEY'))
            ->getToken();

        return (string) $token;
    }

    public function registrars()
    {
        return $this->hasMany(Registrar::class);
    }
}
