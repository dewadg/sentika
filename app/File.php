<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class File extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'paper_upload_id',
        'file_name',
        'rev'
    ];

    protected $dates = ['deleted_at'];

    public static function getRevNumber($upload_id)
    {
    	return File::where('paper_upload_id', $upload_id)->get()->count()+1;
    }

    public static function sanitizeFileName($file_name, $extension)
    {
        $name = basename($file_name, '.' . $extension);
        $length = strlen($name);
        $duplicate_files = self::withTrashed()->whereRaw('LEFT(file_name, ' . $length . ') = "' . $name . '"')->count();

        if ($duplicate_files > 0) {
            return $name . '_' . ($duplicate_files + 1) . '.' . $extension;
        }

        return $file_name;
    }

    public function url()
    {
        return url('/storage/files/' . $this->file_name);
    }
}
