<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Paper extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'registrar_id',
        'verified_by',
        'title',
        'allow_upload'
    ];

    protected $casts = [
        'id' => 'string',
        'allow_upload' => 'boolean'
    ];

    public $incrementing = false;

    protected $dates = ['deleted_at'];

    public static function createId()
    {
        $paper = Paper::withTrashed()->orderBy('created_at', 'desc')->first();

        if ($paper) {
            $id = (int)substr($paper->id, -3);
            $id++;

            if ($id < 10) {
                return "S00".$id;
            }else{
                if ($id < 100) {
                    return "S0".$id;
                }else{
                    return "S".$id;
                }
            }
        }else{
            return "S001";
        }
    }
    
    public function registrar()
    {
        return $this->belongsTo(Registrar::class);
    }

    public function verifiedBy()
    {
        return $this->belongsTo(User::class, 'verified_by');
    }

    public function logs()
    {
        return $this->belongsToMany(PaperStatus::class, 'paper_logs', 'paper_id', 'paper_status_id')
                ->withPivot('related_user_id', 'created_at')
                ->orderBy('paper_logs.paper_status_id', 'DESC');
    }

    public function additionalAuthors()
    {
        return $this->belongsToMany(Registrar::class, 'additional_authors', 'paper_id', 'registrar_id');
    }

    public function review()
    {
        return $this->hasOne(Review::class, 'paper_id');
    }

    public function letter()
    {
        return $this->hasOne(PaperLetter::class, 'paper_id');
    }
}
