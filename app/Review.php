<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Review extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'paper_id',
        'reviewed_by',
        'verified_by',
        'comment'
    ];

    protected $dates = ['deleted_at'];

    public function paper()
    {
        return $this->belongsTo(Paper::class);
    }

    public function reviewed_by()
    {
        return $this->belongsTo(User::class, 'reviewed_by');
    }
}
