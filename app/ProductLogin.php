<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductLogin extends Model
{
    protected $table = 'products';
    protected $hidden = ['pivot'];
}
