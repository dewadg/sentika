import './bootstrap';
import App from './components/App/App';
import router from './routes/App/router';
import store from './stores/store';

const app = new Vue({
  el: '#app',
  router,
  store,
  
  render: h => h(App),
});
