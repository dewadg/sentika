import VueRouter from 'vue-router';

import store from '../../stores/store';
import jwt from 'jsonwebtoken';

import DashboardIndex from '../../components/App/Dashboard/DashboardIndex';
import ChangePassword from '../../components/App/Profile/ChangePassword';
import PostIndex from '../../components/App/Post/PostIndex';
import PostCreate from '../../components/App/Post/PostCreate';
import PostEdit from '../../components/App/Post/PostEdit';
import RegistrarIndex from '../../components/App/Registrar/RegistrarIndex';
import RegistrarEdit from '../../components/App/Registrar/RegistrarEdit';
import DownloadIndex from '../../components/App/Download/DownloadIndex';
import PaperIndex from '../../components/App/Paper/PaperIndex';
import PaperEdit from '../../components/App/Paper/PaperEdit';
import SubRegistrarIndex from '../../components/App/SubRegistrar/SubRegistrarIndex';
import SubRegistrarEdit from '../../components/App/SubRegistrar/SubRegistrarEdit';
import ReviewIndex from '../../components/App/Review/ReviewIndex';
import TransactionIndex from '../../components/App/Transaction/TransactionIndex';
import TransactionDetail from '../../components/App/Transaction/TransactionDetail';

/**
 * Roles:
 *
 * 1 => Administrator
 * 2 => Reviewer
 * 3 => Author
 * 4 => Registrar
 */

const router = new VueRouter({
  routes: [
    { name: 'index', path: '/', component: DashboardIndex, meta: { role: [1, 2] } },
    { name: 'profile.change_password', path: '/profile/change-password', component: ChangePassword, meta: { role: [1, 2, 3, 4] } },
    { name: 'posts.index', path: '/posts', component: PostIndex, meta: { role: [1] } },
    { name: 'posts.create', path: '/posts/new', component: PostCreate, meta: { role: [1] } },
    { name: 'posts.edit', path: '/posts/:id', component: PostEdit, meta: { role: [1] } },
    { name: 'registrars.index', path: '/registrars', component: RegistrarIndex, meta: { role: [1] } },
    { name: 'registrars.edit', path: '/registrars/:id', component: RegistrarEdit, meta: { role: [1, 4] } },
    { name: 'downloads.index', path: '/downloads', component: DownloadIndex, meta: { role: [1] } },
    { name: 'papers.index', path: '/papers', component: PaperIndex, meta: { role: [1, 2, 4], products: [1, 2] }, },
    { name: 'papers.edit', path: '/papers/:id', component: PaperEdit, meta: { role: [1, 2, 4], products: [1, 2] } },
    { name: 'sub_registrars.index', path: '/sub-registrars', component: SubRegistrarIndex, meta: { role: [1, 4], products: [1, 2] } },
    { name: 'sub_registrars.edit', path: '/sub-registrars/:id', component: SubRegistrarEdit, meta: { role: [1, 4], products: [1, 2] } },
    { name: 'review.index', path: '/review', component: ReviewIndex, meta: { role: [1, 2, 4], products: [1, 2] } },
    { name: 'transaction.index', path: '/transaction', component: TransactionIndex, meta: { role: [1] } },
    { name: 'transaction.detail', path: '/transaction/:id', component: TransactionDetail, meta: { role: [1] } },
  ],
});

const decodeToken = () => {
    const token = document.querySelector('meta[name="jwt"]').content;

    if (token) {
      const decodedToken = jwt.decode(token, { complete: true });
      store.commit('loggedUser/setSource', decodedToken.payload.user);
      store.commit('registrar/setLogged', decodedToken.payload.registrar);
    }
}

const doBeforeEach = (to, from, next) => {
    decodeToken();

    let role = store.getters['loggedUser/role'];
    let registrar = store.getters['registrar/logged'];
    let passed = false;

    if(to.meta.role.indexOf(role.id) != -1) {
        if (role.id == 4 && to.meta.products) {
            registrar.products_login.forEach((e) => {
              if(to.meta.products.indexOf(e.id) != -1) {
                passed = true;
              }
            });

            if (passed) {
                next();
            }else{
                next({'path': '/registrars/'+registrar.id});
            }
        }else{
            next();
        }
    }else{
        switch(role.id) {
            case 2:
                break;
            case 3:
                break;
            case 4:
                next({'path': '/registrars/'+registrar.id});
                break;
            default:
                next(false);
        }
    }
}

router.beforeEach(doBeforeEach);

export default router;