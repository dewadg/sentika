import VueRouter from 'vue-router';
import Home from '../../components/Home/Page/Home';
import Post from '../../components/Home/Page/Post';
import Registration from '../../components/Home/Page/Registration';
import Proceeding from '../../components/Home/Page/Proceeding';
import PublicRegistrars from '../../components/Home/Page/PublicRegistrars';
import PublicAuthors from '../../components/Home/Page/PublicAuthors';

const router = new VueRouter({
  routes: [
		{ name: 'index', path: '/', component: Home },
		{ name: 'registration', path: '/registration', component: Registration },
		{ name: 'proceeding', path: '/proceeding', component: Proceeding },
		{ name: 'public_registrars', path: '/public-registrars', component: PublicRegistrars },
		{ name: 'public_authors', path: '/public-authors', component: PublicAuthors },
		{ name: 'post', path: '/:slug', component: Post },
  ],
});

export default router;