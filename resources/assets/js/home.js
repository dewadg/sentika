import './bootstrap';
import Home from './components/Home/Home';
import router from './routes/Home/router';
import store from './stores/store';

const home = new Vue({
	el: '#home',
	router,
	store,

	render: h => h(Home)
});
