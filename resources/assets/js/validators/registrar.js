import {
  required,
  email,
  maxLength,
  sameAs,
} from 'vuelidate/lib/validators';
  
export default {
  registrar: {
    full_name: {
      required,
    },
    phone: {
      required,
      maxLength: maxLength(12)
    },
    email: {
      required,
      email
    },
    password: {
      required,
    },
    password_conf: {
      required,
      sameAsPassword: sameAs('password'),
    },
    address: {
      required,
    },
    organization: {
      required,
    },
    organization_address: {
      required,
    },
  },
};