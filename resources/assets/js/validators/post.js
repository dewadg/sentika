import { required } from 'vuelidate/lib/validators';
  
export default {
  post: {
    title: {
      required,
    },
    hidden: {
      required,
    },
  },
};