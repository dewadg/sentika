import { 
  required,
  requiredIf,
  sameAs,
} from 'vuelidate/lib/validators';

export default {
  password: {
    required,
  },
  new_password: {
   required,
  },
  new_password_conf: {
    required: requiredIf(model => {
      return model.new_password !== '';
    }),
    sameAsPassword: sameAs('new_password'),
  },
};