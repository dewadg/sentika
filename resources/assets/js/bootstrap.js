import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import Vuelidate from 'vuelidate';
import $ from 'jquery';
import http from './services/http';
import VueSweetAlert from 'vue-sweetalert'

Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(Vuelidate);
Vue.use(VueSweetAlert);

window.Vue = Vue;
window.$ = window.jQuery = $;

http.init();
