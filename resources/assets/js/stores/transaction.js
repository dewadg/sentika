import http from '../services/http';

const emptyTransaction = () => ({
  id: null,
  user_id: '',
  registrar_id: '',
  verified_by: '',
  total: '',
  payment_proof_id: '',
});

export default {
  namespaced: true,
  state: {
    data: [],
    active: emptyTransaction(),
  },

  mutations: {
    setSource(state, source) {
      state.data = source;
    },
    setActive(state, data) {
      state.active = data;
    },
  },

  actions: {
    fetchAll(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get('transactions', successCallback, errorCallback);
      });
    },

    store(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setActive', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.post('transactions', payload, successCallback, errorCallback);
      });
    },

    fetch(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setActive', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get(`transactions/${id}`, successCallback, errorCallback);
      });
    },

    update(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setActive', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.patch(`transactions/${payload.id}`, payload, successCallback, errorCallback);
      });
    },

    destroy(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setActive', {});
            resolve();
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.delete(`transactions/${id}`, successCallback, errorCallback);
      });
    }
  },
};