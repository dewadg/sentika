import http from '../services/http';

const emptyPost = () => ({
  id: null,
  title: '',
  slug: '',
  content: '',
  hidden: '0',
});

export default {
  namespaced: true,
  state: {
    data: [],
    active: emptyPost(),
  },

  mutations: {
    setSource(state, source) {
      state.data = source;
    },

    setActivePost(state, post) {
      state.active = post;
    },

    clearActivePost(state) {
      state.active = emptyPost();
    },

    addPost(state, post) {
      state.data.push(post);
    },
  },

  actions: {
    fetchAll(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get('posts', successCallback, errorCallback);
      });
    },

    store(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('fetchAll')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err);
              });
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.post('posts', payload, successCallback, errorCallback);
      });
    },

    fetch(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get(`posts/${id}`, successCallback, errorCallback);
      });
    },

    update(context, payload) {
      return new Promise((resolve, reject) => {
        const data = {
          title: payload.title,
          slug: payload.slug,
          content: payload.content,
          hidden: payload.hidden,
        };

        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('fetchAll')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err);
              });
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.patch(`posts/${payload.id}`, data, successCallback, errorCallback);
      });
    },

    destroy(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('fetchAll')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err);
              });
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.delete(`posts/${id}`, successCallback, errorCallback);
      });
    }
  },
};