import Vuex from 'vuex';
import app from './app';
import loggedUser from './loggedUser';
import post from './post';
import registrar from './registrar';
import product from './product';
import download from './download';
import transaction from './transaction';
import paper from './paper';
import review from './review';

const store = new Vuex.Store({
  modules: {
    app,
    loggedUser,
    post,
    registrar,
    product,
    download,
    transaction,
    paper,
    review,
  },
});

export default store;
