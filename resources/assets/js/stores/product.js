import http from '../services/http';

export default {
  namespaced: true,
  state: {
    data: [],
  },

  mutations: {
    setSource(state, source) {
      state.data = source;
    },
  },

  getters: {
    baseProducts(state) {
      const baseProductIds = [1, 2, 3];

      return state.data.filter(item => baseProductIds.indexOf(item.id) > -1)
    },

    additionalProducts(state) {
      const baseProductIds = [1, 2, 3];

      return state.data.filter(item => baseProductIds.indexOf(item.id) < 0)
    },
  },

  actions: {
    fetchAll(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get('products', successCallback, errorCallback);
      });
    },
  },
};