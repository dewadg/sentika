import http from '../services/http';

const emptyPaper = () => ({
    id: '',
    registrar_id: null,
    verified_by: null,
    title: '',
   	allow_upload: true,
    uploads: [],
    logs: [],
});

const emptyStatuses = () => ([]);

export default {
	namespaced: true,
  state: {
    data: [],
    active: emptyPaper(),
    statuses: emptyStatuses(),
    count: 1,
  },

  mutations: {
    setSource(state, source) {
      state.data = source;
    },
    setActive(state, data) {
      state.active = data;
    },
    setStatuses(state, source) {
      state.statuses = source;
    },
    setCount(state, data) {
      state.count = data;
    },
    setActiveAuthors(state, data) {
      state.active.additional_authors = data;
    },
    setActiveLogs(state, data) {
      state.active.logs = data;
    },
  },

  actions: {
  	fetchAll(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get('papers', successCallback, errorCallback);
      });
    },

    fetch(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setActive', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get(`papers/${id}`, successCallback, errorCallback);
      });
    },

    fetchStatuses(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setStatuses', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get('papers-statuses', successCallback, errorCallback);
      });
    },

    fetchByRegistrar(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get(`papers/by-registrar/${id}`, successCallback, errorCallback);
      });
    },

    fetchProceeding(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get('papers-proceeding', successCallback, errorCallback);
      });
    },

    countByRegistrar(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setCount', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get(`papers/count-by-registrar/${id}`, successCallback, errorCallback);
      });
    },

    store(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setActive', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.post('papers', payload, successCallback, errorCallback);
      });
    },

    storeLog(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setActive', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.post('papers-logs', payload, successCallback, errorCallback);
      });
    },

    storeAuthor(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setActive', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.post('papers-authors', payload, successCallback, errorCallback);
      });
    },

    upload(context, [payload, id]) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setActive', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.post(`papers/${id}`, payload, successCallback, errorCallback);
      });
    },

    uploadLetter(context, [payload, id]) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setActive', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.post(`paper-letters/${id}`, payload, successCallback, errorCallback);
      });
    },

    update(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setActive', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.patch(`papers/${payload.id}`, payload, successCallback, errorCallback);
      });
    },

    updateAuthor(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setActiveAuthors', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.patch(`papers-authors/${payload.id}`, payload, successCallback, errorCallback);
      });
    },

    updateLog(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setActiveLogs', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.patch(`papers-logs/${payload.id}`, payload, successCallback, errorCallback);
      });
    },

    destroy(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve();
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.delete(`papers/${id}`, successCallback, errorCallback);
      });
    },

    destroyAuthor(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve();
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.delete(`papers-authors/${id}`, successCallback, errorCallback);
      });
    },

    destroyLog(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve();
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.delete(`papers-logs/${id}`, successCallback, errorCallback);
      });
    },

    destroyLetter(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve();
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.delete(`paper-letters/${id}`, successCallback, errorCallback);
      });
    },

    sendRevisionLetter(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };
        http.get(`papers/${id}/send-revision-email`, successCallback, errorCallback);
      });
    },

    sendRejectionLetter(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };
        http.get(`papers/${id}/send-rejection-email`, successCallback, errorCallback);
      });
    },

    sendLoa(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };
        http.get(`papers/${id}/send-loa-email`, successCallback, errorCallback);
      });
    },
  },
}