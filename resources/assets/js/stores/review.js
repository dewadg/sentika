import http from '../services/http';

const emptyReview = () => ({
  id: null,
  paper_id: null,
  reviewed_by: null,
  verified_by: null,
  comment: '',
});

export default {
  namespaced: true,
  state: {
    data: [],
    active: emptyReview(),
  },

  mutations: {
    setSource(state, source) {
      state.data = source;
    },

    setActive(state, data) {
      state.active = data;
    },
  },

  actions: {
    setEmpty(context) {
      context.commit('setActive', emptyReview());
    },

    fetchAll(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get('reviews', successCallback, errorCallback);
      });
    },

    store(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setActive', res.data);
            resolve(res.data)
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.post('reviews', payload, successCallback, errorCallback);
      });
    },

    fetch(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setActive', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get(`reviews/${id}`, successCallback, errorCallback);
      });
    },

    fetchByRegistrar(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get(`reviews/by-registrar/${id}`, successCallback, errorCallback);
      });
    },

    update(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setActive', res.data);
            resolve(res.data)
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.patch(`reviews/${payload.id}`, payload, successCallback, errorCallback);
      });
    },
  },
};