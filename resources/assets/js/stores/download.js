import http from '../services/http';

export default {
  namespaced: true,
  state: {
    data: [],
  },

  mutations: {
    setSource(state, source) {
      state.data = source;
    },
  },

  actions: {
    fetchAll(context) {
      return new Promise((resolve, reject) => {
        const successCallback = (res) => {
          if (res.status === 200) {
            context.commit('setSource', res.data);
            resolve();
          }
        };

        const errorCallback = (err) => {
          const errData = Object.assign({}, err.response);
          reject(errData);
        };

        http.get('downloads', successCallback, errorCallback);
      });
    },

    store(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = (res) => {
          if (res.status === 201) {
            resolve();
          }
        };

        const errorCallback = (err) => {
          const errData = Object.assign({}, err.response);
          reject(errData);
        };

        http.post('downloads', payload, successCallback, errorCallback);
      });
    },

    destroy(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = (res) => {
          if (res.status === 200) {
            resolve();
          }
        };

        const errorCallback = (err) => {
          const errData = Object.assign({}, err.response);
          reject(errData);
        };

        http.delete(`downloads/${id}`, successCallback, errorCallback);
      });
    },
  },
};