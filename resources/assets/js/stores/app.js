export default {
  state: window.__INITIAL_STATE__,

  getters: {
    appName: state => state.name,
    appAbbr: state => state.abbr,
    appUrl: state => state.url,
    csrf: state => state.csrf,
  },
};