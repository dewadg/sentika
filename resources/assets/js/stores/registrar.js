import http from '../services/http';

const emptyRegistrar = () => ({
  id: null,
  full_name: '',
  phone: '',
  address: '',
  organization: '',
  organization_address: '',
  products: [],
  sub_registrars: [],
  user: {},
  registered_by: {},
});

export default {
  namespaced: true,
  state: {
    data: [],
    active: emptyRegistrar(),
    logged: emptyRegistrar(),
    count: 0,
  },

  getters: {
    logged: state => state.logged,
  },

  mutations: {
    setSource(state, source) {
      state.data = source;
    },

    setActive(state, data) {
      state.active = data;
    },

    setLogged(state, data) {
      state.logged = data;
    },

    setCount(state, data) {
      state.count = data;
    },
  },

  actions: {
    fetchAll(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get('registrars', successCallback, errorCallback);
      });
    },

    fetchAllSubRegistrar(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get('sub-registrars', successCallback, errorCallback);
      });
    },

    fetch(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setActive', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get(`registrars/${id}`, successCallback, errorCallback);
      });
    },

    fetchPublicRegistrars(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get('public-registrars', successCallback, errorCallback);
      });
    },

    fetchPublicAuthors(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get('public-authors', successCallback, errorCallback);
      });
    },

    fetchSubRegistrarByRegistrar(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get(`sub-registrars-by-registrar/${id}`, successCallback, errorCallback);
      });
    },

    countSubRegistrarByRegistrar(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setCount', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get(`count-sub-registrars-by-registrar/${id}`, successCallback, errorCallback);
      });
    },

    store(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setActive', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.post('registrars', payload, successCallback, errorCallback);
      });
    },

    update(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            //context.dispatch('fetchAll');
            context.commit('setActive', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.patch(`registrars/${payload.id}`, payload, successCallback, errorCallback);
      });
    },

    destroy(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve();
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.delete(`registrars/${id}`, successCallback, errorCallback);
      });
    },
  },
};