import http from '../services/http';

export default {
  namespaced: true,
  state: {
    data: {},
  },

  getters: {
    email: state => state.data.email,
    role: state => state.data.role,
  },

  mutations: {
    setSource(state, source) {
      state.data = source;
    },
  },

  actions: {
    changePassword(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.post('profile/change-password', payload, successCallback, errorCallback);
      });
    },
  }
};