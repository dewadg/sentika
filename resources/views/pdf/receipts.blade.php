<!DOCTYPE html>
<html>
<head>
  <title>details</title>
  <style>
    body {
      font-family: "Arial", sans-serif;
    }

    .receipt {
      border: 5px solid #000;
      padding: 15px;
      margin-bottom: 25px;
    }

    .details {
      width: 100%;
      border: 1px solid #000;
      border-collapse: collapse;
      margin-bottom: 20px;
    }

    .details thead tr th.details-header {
      height: 75px;
      border-bottom: 2px solid #000;
    }

    .details thead tr th.details-header img {
      display: inline-block;
      width: 70px;
      height: auto;
      margin: 7.5px 0 0 10px;
    }

    .details thead tr th.details-header .title {
      width: 60%;
      display: inline-block;
      margin: 7.5px 0 0 10px;
      border-right: 1px solid #000;
    }

    .details thead tr th.details-header .title h2,
    .details thead tr th.details-header .desc span {
      font-size: 22px;
      margin: 0;
    }

    .details thead tr th.details-header .title span {
      display: block;
      font-size: 10px;
      font-weight: normal;
    }

    .details thead tr th.details-header .desc {
      display: inline-block;
      margin: -5px 0 0 10px;
    }

    .details thead tr th.details-header .desc span {
      display: inline-block;
    }

    .details tbody tr > th,
    .details tbody tr > td {
      padding: 10px;
      border-bottom: 1px solid #000;
      font-size: 12px;
      background: #eee;
    }
    
    .details tbody tr > th {
      width: 30%;
      font-weight: normal;
    }

    .details tbody tr > td.separator {
      width: 10px;
    }

    .details tbody tr > td.name {
      font-weight: bold;
      text-transform: uppercase;
    }

    .details tbody tr > td.in-words {
      text-transform: capitalize;
    }

    .summary {
      border-collapse: collapse;
      width: 100%;
    }

    .total {
      display: inline-block;
      border: 1px solid #000;
      padding: 10px;
      text-transform: uppercase;
      font-weight: bold;
    }

    .signer {
      width: 100%;
      border: 1px solid #000;
      text-align: center;
      font-size: 12px;
      padding: 5px;
    }
  </style>
</head>

<body>
  @foreach($transactions as $transaction)
    <div class="receipt">
      <table class="details">
        <thead>
          <tr>
            <th class="details-header" colspan="3">
              <img src="{{ public_path() . '/images/logo.jpeg' }}">
              <div class="title">
                <h2>SENTIKA 2019</h2>
                <span>Seminar Nasional Teknologi Informasi dan Komunikasi</span>
                <span>13-14 Maret 2019</span>
              </div>
              <div class="desc">
                <span>KUITANSI</span>
              </div>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th>Telah Terima Dari</th>
            <td class="separator">:</td>
            <td class="name">{{ $transaction->registrar->full_name }}</td>
          </tr>
          <tr>
            <th>ID Peserta</th>
            <td class="separator">:</td>
            <td class="name">{{ $transaction->registrar->id }}</td>
          </tr>
          <tr>
            <th>Jumlah</th>
            <td class="separator">:</td>
            <td class="in-words">{{ $transaction->total_in_words }}</td>
          </tr>
          <tr>
            <th>Guna Membayar</th>
            <td class="separator">:</td>
            <td>{{ $description }}</td>
          </tr>
        </tbody>
      </table>
      <table class="summary">
        <tbody>
          <tr>
            <td width="60%">
              <div class="total">
                Terbilang Rp {{ number_format($transaction->total, 0, 0, '.') . ',-' }}
              </div>
            </td>
            <td width="40%">
              <div class="signer">
                Penerima<br><br><br><br>
                Panitia SENTIKA 2019
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  @endforeach
</body>
</html>