<!DOCTYPE html>
<html>
<head>
  <title>loa</title>
  <style>
    body {
      font-family: "Arial", sans-serif;
      font-size: 16px;
    }

    .header {
      width: 100%;
      border: none;
      border-bottom: 1px solid #000;
      border-collapse: collapse;
    }

    .header tbody tr td {
      font-size: 16px;
      font-weight: bold;
    }

    .header tbody tr td.title {
      font-size: 20px;
    }

    .header tbody tr td.bottom {
      padding-bottom: 25px;
    }

    .content h2 {
      font-size: 20px;
      text-decoration: underline;
      text-align: center;
    }

    .content ol > li {
      margin-bottom: 10px;
    }

    .signature {
      position: relative;
      margin-top: 50;
    }

    .signature .stamp,
    .signature .signer,
    .signature .text {
      position: absolute;
    } 

    .signature .stamp {
      width: 100px;
      top: 25px;
      right: 100px;
    }

    .signature .signer {
      width: 120px;
      top: 25px;
      right: 50px;
    }

    .signature .text {
      right: 0;
      z-index: 999;
    }
  </style>
</head>

<body>
  <table class="header">
    <tbody>
      <tr>
        <td width="15%" rowspan="4">
          <img src="{{ public_path() . '/images/logo.png' }}">
        </td>
        <td class="title">SENTIKA 2019</td>
      </tr>
      <tr>
        <td>Seminar Nasional Teknologi Informasi dan Komunikasi Tahun 2019</td>
      </tr>
      <tr>
        <td>13-14 Maret 2019</td>
      </tr>
      <tr>
        <td class="bottom">Platinum Adisucipto Hotel & Conference Center</td>
      </tr>
    </tbody>
  </table>
  <div class="content">
    <h2>LETTER OF ACCEPTANCE</h2>
    <p>
      Kepada Yth.<br>
      <strong>{{ $paper->registrar->full_name }}</strong><br>
      <strong>{{ $paper->registrar->organization }}</strong><br>
    </p>
    <p>
      Dengan hormat,<br>
      Bersama dengan surat ini, kami selaku Panitia Seminar Nasional Teknologi Informasi dan Komunikasi SENTIKA 2019 menyatakan
      bahwa makalah yang berjudul:<br>
      <center><strong class="paper">{{ $paper->title }}</strong><br></center>
      <br>dinyatakan DITERIMA dan SIAP untuk dimuat dalam Prosiding SENTIKA 2019.
    </p>
    <p>Demikian surat ini kami sampaikan, atas perhatiannya kami ucapkan terima kasih.</p>

    <div class="signature" align="right">
      <div class="text">
        Yogyakarta, {{ date('j F Y') }}<br>
        Ketua Panitia,<br><br><br><br><br>
        Stephanie Pamela Adithama, S.T., M.T.
      </div>
      <img class="stamp" src="{{ public_path() . '/images/stamp.png' }}">
      <img class="signer" src="{{ public_path() . '/images/sign.png' }}">
    </div>
  </div>
</body>
</html>