<!DOCTYPE html>
<html>
<head>
  <title>Status Paper SENTIKA UAJY 2019</title>
</head>

<body>
  <h3>SENTIKA UAJY 2019</h3>
  <p>Kepada Yth.<br>
  <strong>{{ $paper->registrar->full_name }}</strong><br>
  <strong>{{ $paper->registrar->organization }}</strong></p>
  <p>Dengan hormat,<br>
  Bersama dengan surat ini, kami selaku Panitia Seminar Nasional Teknologi Informasi dan Komunikasi SENTIKA 2019 menyatakan bahwa makalah yang berjudul:</p>
  <p align="center"><strong>{{ $paper->title }}</strong></p>
  <p>Dengan kode makalah: {{ $paper->id }}</p>
  <p>sudah kami proses dan dinyatakan</p>
  <p><strong>Diterima dengan revisi</strong> yaitu:</p>
  
  {!! ! is_null($paper->review) ? $paper->review->comment : '---' !!}
  
  <p>Camera ready paper harus telah direvisi sesuai dengan hasil dari reviewer dan harus disesuaikan dengan format template SENTIKA 2019. <strong>Jika camera ready paper tidak direvisi atau pun tidak sesuai dengan template yang ada maka camera ready paper tidak dapat kami muat dalam prosiding.</strong></p>

  <p>Upload kembali Camera ready paper di website SENTIKA 2019 (<a href="http://sentika.uajy.ac.id">http://sentika.uajy.ac.id</a>) paling lambat tanggal <strong>26 Februari 2019</strong> dengan template seperti pada <a href="http://sentika.uajy.ac.id/storage/downloads/TemplateSENTIKA.doc">http://sentika.uajy.ac.id/storage/downloads/TemplateSENTIKA.doc</a></p>

  <p>Upload Surat Pernyataan yang sudah ditandatangani di atas materai di website SENTIKA 2019 (<a href="http://sentika.uajy.ac.id">http://sentika.uajy.ac.id</a>) paling lambat tanggal <strong>26 Februari 2019</strong> dengan format seperti pada <a href="http://sentika.uajy.ac.id/storage/downloads/Surat%20Pernyataan%20Plagiarism%20SENTIKA%202019.doc">http://sentika.uajy.ac.id/storage/downloads/Surat%20Pernyataan%20Plagiarism%20SENTIKA%202019.doc</a></p>

  <p>Makalah yang sudah dikirimkan ulang dan direvisi dimuat dalam prosiding setelah pemakalah meng-upload bukti pembayaran melalui website SENTIKA 2019 (<a href="http://sentika.uajy.ac.id">http://sentika.uajy.ac.id</a>) sebesar <strong>Rp{{ number_format($total, 0, '.', '.') }}</strong> , dengan perincian:</p>

  <table>
    <tbody>
      @foreach($paper->registrar->products as $product)
        <tr>
          <td>{{ $product->name }} x {{ $product->pivot->qty }}</td>
          <td>:</td>
          <td><strong>Rp{{ number_format($product->price * $product->pivot->qty, 0, '.', '.') }}</strong></td>
        </tr>
      @endforeach
    </tbody>
  </table>

  <p>ke Bank CIMB NIAGA no. rek. 703095534300 Atas Nama Stephanie Pamela Adithama paling lambat tanggal <strong>26 Februari 2019.</strong></p>

  <p>Bagi yang tidak dapat hadir pada presentasi seminar, harap membalas email ini dan mengirimkan alamat pengiriman yang jelas
  dan lengkap. <i>Biaya pengiriman sertifikat dan prosiding akan diberikan kemudian</i>.</p>

  <p>Atas perhatian dan partisipasinya kami ucapkan terima kasih.</p>

  <p><strong>CATATAN: Pengumuman yang dikirimkan melalui email ini bukan merupakan Letter of Acceptance. Letter of Acceptance akan diberikan pada saat camera ready paper, surat pernyataan, dan konfirmasi pembayaran telah kami terima.</strong></p>
</body>
</html>