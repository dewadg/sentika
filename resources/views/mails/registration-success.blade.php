<!DOCTYPE html>
<html>
<head>
  <title>Pendaftaran SENTIKA UAJY 2019 Berhasil</title>
</head>

<body>
  <h3>SENTIKA UAJY 2019</h3>
  <p>Selamat, pendaftaran peserta SENTIKA UAJY 2019 berhasil dengan data sebagai berikut:</p>
  <table border="0">
    <tbody>
      <tr>
        <td width="200">Nama Lengkap</td>
        <td width="50">:</td>
        <td>{{ $registrar->full_name }}</td>
      </tr>
      <tr>
        <td>Email</td>
        <td>:</td>
        <td>{{ $registrar->user->email }}</td>
      </tr>
      <tr>
        <td>Telpon</td>
        <td>:</td>
        <td>{{ $registrar->phone }}</td>
      </tr>
      <tr>
        <td>Alamat</td>
        <td>:</td>
        <td>{{ $registrar->address }}</td>
      </tr>
      @if(! empty($registrar->organization))
        <tr>
          <td>Institusi</td>
          <td>:</td>
          <td>{{ $registrar->organization }}</td>
        </tr>
        <tr>
          <td>Alamat Institusi</td>
          <td>:</td>
          <td>{{ $registrar->organization_address }}</td>
        </tr>
      @endif
      <tr>
        <td>Paket</td>
        <td>:</td>
        <td>{{ implode(', ', $products) }}</td>
      </tr>
    </tbody>
  </table>
  <p>Untuk keperluan administrasi, Anda dapat mengakses client-area pada <a href="{{ route('index.login') }}">link ini</a>.</p>
</body>
</html>