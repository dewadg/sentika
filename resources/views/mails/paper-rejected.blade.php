<!DOCTYPE html>
<html>
<head>
  <title>Status Paper SENTIKA UAJY 2019</title>
</head>

<body>
  <h3>SENTIKA UAJY 2019</h3>
  <p>Kepada Yth.<br>
  <strong>{{ $paper->registrar->full_name }}</strong><br>
  <strong>{{ $paper->registrar->organization }}</strong></p>
  <p>Dengan hormat,<br>
  Bersama dengan surat ini, kami selaku Panitia Seminar Nasional Teknologi Informasi dan Komunikasi SENTIKA 2019 menyatakan bahwa makalah yang berjudul:</p>
  <p align="center"><strong>{{ $paper->title }}</strong></p>
  <p>Dengan kode makalah: {{ $paper->id }}</p>
  <p>sudah kami proses dan dengan berat hati dinyatakan</p>
  <p><strong>Ditolak</strong></p>
  <p>dengan alasan dari reviewer yaitu:</p>
  
  {!! ! is_null($paper->review) ? $paper->review->comment : '---' !!}
  
  <p>Atas perhatian dan partisipasinya kami ucapkan terima kasih.</p>
</body>
</html>