<!DOCTYPE html>
<html>
<head>
  <title>LOA SENTIKA UAJY 2019</title>
</head>

<body>
  <h3>SENTIKA UAJY 2019</h3>
  <p>Kepada Yth.<br>
  <strong>{{ $paper->registrar->full_name }}</strong><br>
  <strong>{{ $paper->registrar->organization }}</strong></p>
  <p>Dengan hormat,<br>
  Bersama dengan surat ini, kami selaku Panitia Seminar Nasional Teknologi Informasi dan Komunikasi SENTIKA 2019 menginformasikan bahwa kami telah menerima camera ready paper dan bukti pembayaran. Berikut kami kirimkan Letter of Acceptance (LOA) untuk makalah Anda. Kami menunggu kehadirannya pada acara SENTIKA 2018 yang diselenggarakan pada tanggal 13-14 Maret 2019 di Platinum Adisucipto Hotel & Conference Center.</p>
  <p>Atas perhatian dan partisipasinya kami ucapkan terima kasih.</p>
</body>
</html>