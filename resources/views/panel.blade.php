<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ env('APP_NAME') }}</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta content="{{ $jwt }}" name="jwt">
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-yellow sidebar-mini">
<div id="app"></div>
<script>
window.__INITIAL_STATE__ = {
  name: '{{ env('APP_NAME') }}',
  abbr: '{{ env('APP_ABBR') }}',
  url: '{{ env('APP_URL') }}',
  csrf: '{{ csrf_token() }}'
};
</script>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
