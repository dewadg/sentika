<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegistrationSuccess;
use App\Registrar;

class MailSendingTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRegistrationMail()
    {
        $registrar = Registrar::with(['user', 'products'])->find(2);

        Mail::to('sentikatester@mailinator.com')->send(new RegistrationSuccess($registrar));
    }
}
