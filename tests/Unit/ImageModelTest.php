<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Image;

class ImageModelTest extends TestCase
{
    public function testSanitizeFileName()
    {
        $file_name = Image::sanitizeFileName('row-count-synchrome.png', 'png');

        $this->assertEquals($file_name, 'row-count-synchrome_2.png');
    }

    public function testUrl()
    {
        $image = Image::where('file_name', 'row-count-synchrome.png')->first();

        $this->assertEquals('http://localhost/storage/images/row-count-synchrome.png', $image->url());
    }
}
