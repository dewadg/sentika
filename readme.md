## Sentika

Website-web app yang meng-handle penyampaian informasi, pembayaran, administrasi peserta Sentika UAJY.

## Instalasi
1. Instal dependency Composer: `composer install`
2. Lakukan konfigurasi awal di `.env` (acuan dari file `.env.example`)
5. Lakukan migrasi database: `php artisan migrate`
6. Lakukan seeding database: `php artisan db:seed`
7. Instal dependecy NodeJS: `npm install`
8. Lakukan transpilasi aset: `npm run dev` (untuk development), `npm run prod` (untuk production)