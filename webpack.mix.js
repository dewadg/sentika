let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
	// For Panel
	.js('resources/assets/js/app.js', 'public/js')
    .copy('node_modules/jquery/dist/jquery.min.js', 'public/js')
    .copy('node_modules/bootstrap/dist/js/bootstrap.min.js', 'public/js')
    .copy('node_modules/bootstrap/dist/css/bootstrap.min.css', 'public/css')
    .copy('node_modules/admin-lte/dist/css/AdminLTE.min.css', 'public/css')
    .copy('node_modules/font-awesome/css/font-awesome.min.css', 'public/css')
    .copy('node_modules/ionicons/dist/css/ionicons.min.css', 'public/css')
    .copy('node_modules/tinymce/skins/lightgray/skin.min.css', 'public/js/skins/lightgray')
    .copy('node_modules/tinymce/skins/lightgray/content.min.css', 'public/js/skins/lightgray')
    .copy('node_modules/tinymce/skins/lightgray/fonts/tinymce.woff', 'public/js/skins/lightgray/fonts')

    // For Home
    .js('resources/assets/js/home.js', 'public/js')
    .sass('resources/assets/sass/home.scss', 'public/css')
    .copyDirectory('resources/assets/images', 'public/images')
    .copy('resources/lib/vertical-timeline/css/style.css', 'public/css/vertical-timeline.css');
