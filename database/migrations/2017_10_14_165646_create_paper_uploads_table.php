<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaperUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paper_uploads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('paper_id');
            $table->foreign('paper_id')
                ->references('id')->on('papers');
            $table->string('file_name')->unique();
            $table->float('size');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paper_uploads');
    }
}
