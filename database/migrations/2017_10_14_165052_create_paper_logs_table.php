<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaperLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paper_logs', function (Blueprint $table) {
            $table->string('paper_id');
            $table->foreign('paper_id')
                ->references('id')->on('papers');
            $table->unsignedInteger('paper_status_id');
            $table->foreign('paper_status_id')
                ->references('id')->on('paper_statuses');
            $table->unsignedInteger('related_user_id')->nullable();
            $table->foreign('related_user_id')
                ->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paper_logs');
    }
}
