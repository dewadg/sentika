<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('registrar_id');
            $table->foreign('registrar_id')
                ->references('id')->on('registrars');
            $table->unsignedInteger('verified_by')->nullable();
            $table->foreign('verified_by')
                ->references('id')->on('users');
            $table->decimal('total', 15, 2);
            $table->string('payment_proof');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
