<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdditionalAuthorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('additional_authors', function (Blueprint $table) {
            $table->string('paper_id');
            $table->foreign('paper_id')
                ->references('id')->on('papers');
            $table->unsignedInteger('registrar_id');
            $table->foreign('registrar_id')
                ->references('id')->on('registrars');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('additional_authors');
    }
}
