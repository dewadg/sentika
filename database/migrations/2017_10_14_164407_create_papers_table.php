<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePapersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('papers', function (Blueprint $table) {
            $table->string('id');
            $table->primary('id');
            $table->unsignedInteger('registrar_id');
            $table->foreign('registrar_id')
                ->references('id')->on('registrars');
            $table->unsignedInteger('verified_by')->nullable();
            $table->foreign('verified_by')
                ->references('id')->on('users');
            $table->longText('title');
            $table->boolean('allow_upload')->default(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('papers');
    }
}
