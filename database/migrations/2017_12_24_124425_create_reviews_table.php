<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->string('paper_id')->unique();
            $table->foreign('paper_id')
                ->references('id')->on('papers');
            $table->unsignedInteger('reviewed_by')->nullable();
            $table->foreign('reviewed_by')
                ->references('id')->on('users');
            $table->unsignedInteger('verified_by')->nullable();
            $table->foreign('verified_by')
                ->references('id')->on('users');
            $table->longText('comment');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
