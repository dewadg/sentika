<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductRegistrarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_registrar', function (Blueprint $table) {
            $table->unsignedInteger('qty')->nullable()->after('registrar_id');
            $table->decimal('discount', 15, 2)->nullable()->after('qty');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_registrar', function (Blueprint $table) {
            $table->dropColumn('qty');
            $table->dropColumn('discount');
        });
    }
}
