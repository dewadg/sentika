<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    protected $roles = [
        ['id' => 1, 'name' => 'Administrator'],
        ['id' => 2, 'name' => 'Reviewer'],
        ['id' => 3, 'name' => 'Author'],
        ['id' => 4, 'name' => 'Registrar']
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->roles as $role) {
            Role::create($role);
        }
    }
}
