<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder
{
    protected $products = [
        ['id' => 1, 'name' => 'Pemakalah UAJY', 'price' => 800000],
        ['id' => 2, 'name' => 'Pemakalah Non-UAJY', 'price' => 1100000],
        ['id' => 3, 'name' => 'Peserta Seminar', 'price' => 500000],
        ['id' => 4, 'name' => 'Prosiding', 'price' => 175000],
        ['id' => 5, 'name' => 'Tur', 'price' => 800000],
        ['id' => 6, 'name' => 'Peserta Tambahan', 'price' => 500000],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->products as $product) {
            Product::create($product);
        }
    }
}
