<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email' => 'admin@sentika.uajy.ac.id',
            'password' => bcrypt('admin'),
            'role_id' => 1
        ]);

        User::create([
            'email' => 'reviewer1@reviewer.com',
            'password' => bcrypt('reviewer1'),
            'role_id' => 2
        ]);

        User::create([
            'email' => 'reviewer2@reviewer.com',
            'password' => bcrypt('reviewer2'),
            'role_id' => 2
        ]);
    }
}
