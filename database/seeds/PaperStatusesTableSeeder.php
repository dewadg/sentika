<?php

use Illuminate\Database\Seeder;
use App\PaperStatus;

class PaperStatusesTableSeeder extends Seeder
{
    protected $statuses = [
        ['id' => 1, 'name' => 'Unggahan pertama'],
        ['id' => 2, 'name' => 'Dalam proses review'],
        ['id' => 3, 'name' => 'Diterima'],
        ['id' => 4, 'name' => 'Ditolak'],
        ['id' => 5, 'name' => 'Unggahan cam-ready'],
        ['id' => 6, 'name' => 'Cam-ready'],
        ['id' => 7, 'name' => 'Terverifikasi']
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->statuses as $status) {
            PaperStatus::create($status);
        }
    }
}
