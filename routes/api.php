<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->post('registration', 'Api\RegistrationController@register');
Route::middleware('api')->get('products', 'Api\ProductController@index');
Route::middleware('api')->get('posts/by-slug/{slug}', 'Api\PostController@bySlug');
Route::middleware('api')->get('downloads', 'Api\DownloadController@index');

Route::get('papers-proceeding', 'Api\PaperController@getProceeding');
Route::get('public-registrars', 'Api\RegistrarController@getPublicRegistrars');
Route::get('public-authors', 'Api\RegistrarController@getPublicAuthors');

Route::group(['middleware' => ['api', 'jwt']], function () {
    Route::post('profile/change-password', 'Api\ProfileController@changePassword');

    Route::get('posts', 'Api\PostController@index');
    Route::post('posts', 'Api\PostController@store');
    Route::get('posts/{id}', 'Api\PostController@get');
    Route::delete('posts/{id}', 'Api\PostController@destroy');
    Route::patch('posts/{id}', 'Api\PostController@update');

    Route::get('registrars', 'Api\RegistrarController@index');
    Route::post('registrars', 'Api\RegistrarController@store');
    Route::get('registrars/{id}', 'Api\RegistrarController@get');
    Route::patch('registrars/{id}', 'Api\RegistrarController@update');
    Route::delete('registrars/{id}', 'Api\RegistrarController@destroy');

    Route::get('sub-registrars', 'Api\RegistrarController@getAllSubRegistrars');
    Route::get('sub-registrars-by-registrar/{id}', 'Api\RegistrarController@getSubRegistrarsByRegistrar');
    Route::get('count-sub-registrars-by-registrar/{id}', 'Api\RegistrarController@countSubRegistrarsByRegistrar');

    Route::get('images/{id}', 'Api\ImageController@get');
    Route::post('images', 'Api\ImageController@store');

    Route::post('downloads', 'Api\DownloadController@store');
    Route::delete('downloads/{id}', 'Api\DownloadController@destroy');
    
    Route::post('transactions', 'Api\TransactionController@store');
    Route::get('transactions', 'Api\TransactionController@index');
    Route::get('transactions/{id}', 'Api\TransactionController@get');
    Route::patch('transactions/{id}', 'Api\TransactionController@update');
    Route::delete('transactions/{id}', 'Api\TransactionController@destroy');

    Route::get('papers', 'Api\PaperController@index');
    Route::get('papers/{id}', 'Api\PaperController@get');
    Route::get('papers/by-registrar/{id}', 'Api\PaperController@getByRegistrar');
    Route::patch('papers/{id}', 'Api\PaperController@update');
    Route::post('papers', 'Api\PaperController@store');
    Route::post('papers/{id}', 'Api\PaperController@upload');
    Route::delete('papers/{id}', 'Api\PaperController@destroy');
    Route::get('papers/{id}/send-rejection-email', 'Api\PaperController@sendRejectionEmail');
    Route::get('papers/{id}/send-revision-email', 'Api\PaperController@sendRevisionEmail');
    Route::get('papers/{id}/send-loa-email', 'Api\PaperController@sendLoaEmail');
    Route::get('papers-statuses', 'Api\PaperController@getStatuses');
    Route::post('papers-logs', 'Api\PaperController@storeLog');
    Route::patch('papers-logs/{id}', 'Api\PaperController@updateLog');
    Route::get('papers/count-by-registrar/{id}', 'Api\PaperController@countByRegistrar');
    Route::post('papers-authors', 'Api\PaperController@storeAuthor');
    Route::patch('papers-authors/{id}', 'Api\PaperController@updateAuthor');
    Route::delete('papers-authors/{id}', 'Api\PaperController@destroyAuthor');
    Route::delete('papers-logs/{id}', 'Api\PaperController@destroyLog');
    Route::post('paper-letters/{id}', 'Api\PaperController@uploadLetter');
    Route::delete('paper-letters/{id}', 'Api\PaperController@destroyLetter');
    
    Route::get('reviews', 'Api\ReviewController@index');
    Route::get('reviews/{id}', 'Api\ReviewController@get');
    Route::get('reviews/by-registrar/{id}', 'Api\ReviewController@getByRegistrar');
    Route::post('reviews', 'Api\ReviewController@store');
    Route::patch('reviews/{id}', 'Api\ReviewController@update');
});
