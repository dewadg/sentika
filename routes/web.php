<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('index');

Route::get('login', 'IndexController@login')->name('index.login');
Route::post('login', 'AuthController@login')->name('auth.login');
Route::post('logout', 'AuthController@logout')->name('auth.logout');

Route::middleware('auth')->get('panel', 'IndexController@panel')->name('panel');

Route::get('receipts/{id}', 'Api\TransactionController@generateReceipt');

Route::get('test-email-view/{id}', 'Api\PaperController@testEmailView');
Route::get('test-loa-view/{id}', 'Api\PaperController@testLoaView');
